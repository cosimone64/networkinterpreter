(* Author: Cosimo Agati
 * Switch module *)

open Printf
open Concretetypes

(* Receives a packet, performs the desired operation and
 * routes it to destination. Returns a string containing
 * the processed packet and the port the packet was forwarded to. *)
let route packet operation switch_data =
  (* Looks up dest in forwarding_table and returns the next switch
   * itentifier. Raises an exception if it is not found. *)
  let rec lookup dest forwarding_table =
    match forwarding_table with
    | []      -> failwith "Destination address not found in routing table."
    | x :: xs ->
      if (x.dest_addr = dest) && (x.in_use) then x.next_switch
      else lookup dest xs
  in

  (* Checks if the required operation is allowed by the policies. *)
  let rec is_permitted_op op policy_list =
    match policy_list with
    | []      -> true
    | x :: xs -> if x op then is_permitted_op op xs else false
  in

  (* Checks if the packet can be forwarded according to traffic rules. *)
  let rec is_permitted_packet packet blocking_rules =
    match blocking_rules with
    | [] -> true
    | x :: xs -> if not (x packet) then is_permitted_packet packet xs else false
  in

  let processed_packet = operation.op_function packet in
  let table            = switch_data.forwarding_table in
  let dest             = packet.destination           in
  let next_hop         = lookup dest table            in
  if not (is_permitted_op operation switch_data.policies) then
    "Error: invalid operation."
  else if not (is_permitted_packet packet switch_data.blocking_rules) then
    "Error: invalid packet."
  else
    sprintf "payload: %s - hop: %d" processed_packet.payload next_hop
