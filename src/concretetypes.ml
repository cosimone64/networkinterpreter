(* Author: Cosimo Agati
 * Types module *)

(* Internal OCaml type representation *)

type forwarding_rule = {
  dest_addr   : int;
  next_switch : int;
  in_use      : bool;
}

type packet = {
  source      : int;
  destination : int;
  payload     : string;
}

type operation = {
  op_type     : string;
  op_function : packet -> packet;
}

type policy = operation -> bool

type blocking_rule = packet -> bool

type switch_data = {
  forwarding_table : forwarding_rule list;
  policies         : policy list;
  blocking_rules   : blocking_rule list;
}
