(* Author: Cosimo Agati
 * Switch-controller sample interaction module *)

open Concretetypes
open Controllanguage
open Switch
open Interpreter
open Printf

let abstract_table = Table (0, 1, Table (3, 4,
                                         Leaf_routing_rule (2, 5)))

let sample_data = {
  forwarding_table =
    (let result = eval (Install_table (Table (0, 1, Table (3, 4,
                                                           Leaf_routing_rule (2, 5)))))
     in get_concrete_table result);
  policies         = [];
  blocking_rules   = [];
}

let sample_packet = {
  source      = 2;
  destination = 3;
  payload     = "example";
}

let sample_operation = {
  op_type     = "r";
  op_function = fun packet -> packet;
}

let write_operation = {
  op_type     = "w";
  op_function = fun packet -> {
      source      = packet.source;
      destination = packet.destination;
      payload     = packet.payload ^ " appended text"
    }
}

(* main *)
let () =
  (* Routing with a read operation. *)
  Printf.printf "%s\n" (route sample_packet sample_operation sample_data);

  (* Routing with a write operation *)
  Printf.printf "%s\n" (route sample_packet write_operation sample_data);

  (* Dropping a rule and routing with the
   * missing rule. *)
  let sample_data = {
    forwarding_table = (
      let result = eval (Drop_rule (0, 1, abstract_table))
      in get_concrete_table result);
    policies       = [];
    blocking_rules = [];
  } in
  let sample_packet = {
    source      = 0;
    destination = 1;
    payload     = "example";
  } in
  Printf.printf "%s\n" (try
                          route sample_packet sample_operation sample_data;
                        with Failure f -> f);


  (* Deleting a rule and routing with the
   * dropped rule. *)
  let sample_data = {
    forwarding_table =
      (let result = eval (Delete_rule (0, 1, abstract_table)) in
       get_concrete_table result);
    policies       = [];
    blocking_rules = [];
  } in
  let sample_packet = {
    source      = 0;
    destination = 1;
    payload     = "example";
  } in
  Printf.printf "%s\n" (try
                          route sample_packet sample_operation sample_data;
                        with Failure f -> f);

  (* Installing a policy and routing with the applied policy *)
  let sample_data = {
    forwarding_table =
      (let result = eval (Install_table (Table (0, 1, Table (3, 4,
                                                             Leaf_routing_rule (2, 5))))) in
       get_concrete_table result);
    policies       =
      [let result = eval (Install_policy Read_only) in
       get_concrete_policy result];
    blocking_rules = [];
  } in
  let sample_packet = {
    source      = 2;
    destination = 3;
    payload     = "example";
  } in
  (* First an allowed operation... *)
  Printf.printf "%s\n" (try
                          route sample_packet sample_operation sample_data;
                        with Failure f -> f);
  (* ...and then an unallowed one *)
  Printf.printf "%s\n" (try
                          route sample_packet write_operation sample_data;
                        with Failure f -> f);

  (* Installing a blocking rule and routing with the rule in effect *)
  let sample_data = {
    forwarding_table =
      (let result = eval (Install_table (Table (0, 1, Table (3, 4,
                                                             Leaf_routing_rule (2, 5))))) in
       get_concrete_table result);
    policies       = [];
    blocking_rules =
      [let result = eval (Install_block_rule (Block_source 2)) in
       get_concrete_rule result];
  } in
  let sample_packet = {
    source      = 2;
    destination = 3;
    payload     = "example";
  } in
  Printf.printf "%s\n"
    (try route sample_packet sample_operation sample_data;
     with Failure f -> f);

  (* Handling mobility: updating the routing table. *)
  let sample_data = {
    forwarding_table =
      (let result = eval (Handle_mobility (3, 1, (Table (0, 1, Table (3, 4,
                                                                      Leaf_routing_rule (2, 5)))))) in
       get_concrete_table result);
    policies       = [];
    blocking_rules = [];
  } in
  let sample_packet = {
    source      = 2;
    destination = 3;
    payload     = "example";
  } in
  Printf.printf "%s\n"
    (try route sample_packet sample_operation sample_data;
     with Failure f -> f);
