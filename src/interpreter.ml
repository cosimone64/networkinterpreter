(* Author: Cosimo Agati
 * Interpreter module *)

open Printf
open Concretetypes
open Controllanguage

(* The following three functions return the desired internal
 * type from the result of an expression in our control language. *)
let get_concrete_table table =
  match table with
  | Concrete_table t -> t
  | _ -> failwith "Error while processing forwarding table."

let get_concrete_policy policy =
  match policy with
  | Concrete_policy p -> p
  | _ -> failwith "Error while processing policy."

let get_concrete_rule rule =
  match rule with
  | Concrete_blocking_rule r -> r
  | _ -> failwith "Error while processing blocking rule."

(* This is the main interpreter function. It performs a pattern
 * match to determine the structure of the expression, then calls
 * am internal helper function to convert the expression in the
 * desired internal type. *)
let eval exp =
  let rec install_table table =
    match table with
    | Leaf_routing_rule  (dest, hop) ->
      [{dest_addr = dest; next_switch = hop; in_use = true}]
    | Table (dest, hop, xs)          ->
      {dest_addr = dest; next_switch = hop; in_use = true}
      :: (install_table xs) 
  in

  (* Deletes a rule from the table. Returns an identical table, but
   * with the required rule disabled. *)
  let drop_rule rule table = 
    let concrete_table = install_table table
    in
    let concrete_rule  =
      match rule with (dest, hop) ->
        {dest_addr = dest; next_switch = hop; in_use = true}
    in
    let rec drop_rule_aux rule' table' =
      match table' with
      | []      -> table'
      | x :: xs ->
        if x = rule' then drop_rule_aux rule' xs
        else x :: (drop_rule_aux rule' xs)
    in
    drop_rule_aux concrete_rule concrete_table
  in


  (* Deletes a rule from the table. Returns an identical table, but
   * without the rule. *)
  let rec delete_rule rule table =
    let concrete_table = install_table table in
    let concrete_rule  =
      match rule with (dest, hop) ->
        {dest_addr = dest; next_switch = hop; in_use = true}
    in
    let rec delete_rule_aux rule' table' =
      match table' with
      | []      -> table'
      | x :: xs ->
        if x = rule' then {
          dest_addr = x.dest_addr;
          next_switch = x.next_switch;
          in_use = false; } :: (delete_rule_aux rule' xs)
        else x :: (delete_rule_aux rule' xs)
    in
    delete_rule_aux concrete_rule concrete_table
  in

  (* Returns a function representing the desired policy. *)
  let get_policy policy =
    match policy with
    | Read_only  -> fun op -> op.op_type = "r"
    | Write_only -> fun op -> op.op_type = "w"
    | Read_write -> fun op -> op.op_type = "rw"
  in

  (* Returns a function representing the desired rule. *)
  let rec get_blocking_rule rule =
    match rule with
    | Block_source src -> fun packet -> packet.source = src
    | Block_dest dest  -> fun packet -> packet.destination = dest
  in

  (* Changes an entry in the forwarding table to
   * implement mobility. Returns an indentical table with
   * the desired entry modified.*)
  let handle_mobility src new_hop table =
    let concrete_table = install_table table in
    let rec aux_handle src' new_hop' table' =
      match table' with
      | [] -> []
      | x :: xs ->
        if x.dest_addr = src then
          {
            dest_addr   = src;
            next_switch = new_hop;
            in_use      = x.in_use;
          } :: (aux_handle src' new_hop' xs)
        else x :: (aux_handle src' new_hop' xs)
    in
    aux_handle src new_hop concrete_table
  in

  (* Main pattern match. *)
  match exp with
  | Install_table t ->
    Concrete_table (install_table t);
  | Drop_rule (dest, hop, table) ->
    Concrete_table (drop_rule (dest, hop) table)
  | Delete_rule (dest, hop, table) ->
    Concrete_table (delete_rule (dest, hop) table)
  | Install_policy p ->
    Concrete_policy (get_policy p)
  | Install_block_rule rule ->
    Concrete_blocking_rule (get_blocking_rule rule)
  | Handle_mobility (src, new_hop, t) ->
    Concrete_table (handle_mobility src new_hop t)
