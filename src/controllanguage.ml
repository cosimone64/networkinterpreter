(* Author: Cosimo Agati
 * Controller language module *)

(* This module contains the definition of the controller
 * language. By using variant types, it almost looks like
 * a Backus-Naur CFG definition. *)
open Concretetypes

type abstract_table  =
  | Leaf_routing_rule of int * int
  | Table             of int * int * abstract_table

type abstract_policy =
  | Read_write
  | Read_only
  | Write_only

type abstract_blocking_rule =
  | Block_source of int
  | Block_dest   of int

type exp =
  | Install_table      of abstract_table
  | Drop_rule          of int * int * abstract_table
  | Delete_rule        of int * int * abstract_table
  | Install_policy     of abstract_policy
  | Install_block_rule of abstract_blocking_rule
  | Handle_mobility    of int * int * abstract_table

type exp_result =
  | Concrete_table         of forwarding_rule list
  | Concrete_policy        of policy
  | Concrete_blocking_rule of blocking_rule
