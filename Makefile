SRC=src/
all:
	ocamlopt -o test $(SRC)concretetypes.ml $(SRC)controllanguage.ml\
		$(SRC)switch.ml $(SRC)interpreter.ml $(SRC)interaction.ml
clean:
	rm *.cmi *.o *.cmx
	rm test
